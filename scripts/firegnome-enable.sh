#!/bin/bash

THEMEDIRECTORY=/usr/share/themes/Firefox-Gnome-Maia
FIREFOXFOLDER=~/.mozilla/firefox
PROFILENAME=""
GNOMISHEXTRAS=true

# Get options.
while getopts 'f:p:g' flag; do
	case "${flag}" in    
		f) FIREFOXFOLDER="${OPTARG}" ;;
		p) PROFILENAME="${OPTARG}" ;;
		g) GNOMISHEXTRAS=true ;;
	esac
done

if ! [[ -e $FIREFOXFOLDER ]]; 
then
	# Run firefox to create a profile
	echo "creating a profile"
	firefox &
	sleep 1 && pkill firefox
fi
theme_apply() {
	# Enter Firefox profile folder.
	if ! cd $PROFILEFOLDER ; then
		echo "Error entering profile folder."
		return 0
	fi

	echo "Installing theme in $PWD"

	# Create a chrome directory if it doesn't exist.
	mkdir -p chrome
	cd chrome

	# Link the theme directory to the profile
	echo "Linking repo in $PWD"
	#cp -R $THEMEDIRECTORY $PWD
	[[ -e $PWD/firefox-gnome-theme ]] || ln -s $THEMEDIRECTORY/chrome/ $PWD/firefox-gnome-theme

	# Create single-line user CSS files if non-existent or empty.
	[[ -s userChrome.css ]] || echo >> userChrome.css

	# Import this theme at the beginning of the CSS files.
	sed -i '1s/^/@import "firefox-gnome-theme\/userChrome.css";\n/' userChrome.css

	echo "@import \"firefox-gnome-theme\/theme/colors/light-maia.css\";" >> userChrome.css
	echo "@import \"firefox-gnome-theme\/theme/colors/dark-maia.css\";" >> userChrome.css

	# Symlink user.js to firefox-gnome-theme one.
	echo "Set configuration user.js file"
	[[ -e  ../user.js ]] || ln -s chrome/firefox-gnome-theme/configuration/user.js ../user.js

	# If there is no userContent.css, create it
	[[ -s userContent.css ]] || cp /usr/share/themes/Firefox-Gnome-Maia/chrome/userContent.css userContent.css

	echo "Done."
}


# Define profile folder path.
if test -z "$PROFILENAME" 
	then
		for i in $(echo "$FIREFOXFOLDER/*.default*"); do
			PROFILEFOLDER="$i"
			theme_apply
		done
		
	else
		PROFILEFOLDER="$FIREFOXFOLDER/$PROFILENAME"
		theme_apply
fi

if [[ -e ~/.config/autostart/gnomefy-firefox.desktop ]]; then
	echo 'Hidden=true' >> ~/.config/autostart/gnomefy-firefox.desktop
fi

if [[ -e ~/.config/autostart/firefox-theme.desktop ]]; then
	rm -rf ~/.config/autostart/firefox-theme.desktop
fi
